(** Rename an identifier *)

include Refactoring_sigs.Refactoring

val set_from : string -> unit
(** Specify the identifier to be renamed. *)

val set_to : string -> unit
(** Specify the identifier to be substituted. *)