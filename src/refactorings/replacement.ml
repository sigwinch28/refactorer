open Containers

open Compiler
open   Lexing
open   Location

open Ast_lib
open Lib

type payload = string

module OrderKernel = struct
  type t = {
    location : Location.t ;
    payload  : payload ;
  }
  let compare
        { location = { loc_start = { pos_cnum = p ; _ }; _ }; _ }
        { location = { loc_start = { pos_cnum = p'; _ }; _ }; _ } =
    p - p'
end

module Set =
struct
  include Set.Make(OrderKernel)
  class monoid = object
    inherit [t] VisitorsRuntime.monoid
    method private zero = empty
    method private plus = union
  end
  let of_opt =
    function
    | None ->
      empty
    | Some v ->
      singleton v
  (* TODO: check for consistency when adding elements *)
end

include OrderKernel

let mk loc p = { location = loc; payload = p }
let mk_opt loc p =
  (* Do some sanity checking to avoid errors from bad positions introduced
     by PPX - yes, there are lots of these! *)
  let negative_check { pos_lnum; pos_cnum; pos_bol; _ } =
    pos_lnum < 0 || pos_cnum < 0 || pos_bol < 0 in
  let bol_check { pos_cnum; pos_bol; _ } =
    pos_bol > pos_cnum in
  match loc with
  | { loc_start; loc_end; _ }
      when
        negative_check loc_start || negative_check loc_end ||
        bol_check loc_start || bol_check loc_end ||
        loc_end.pos_cnum < loc_start.pos_cnum ->
    let () = Logging.err @@ fun _f -> _f
      ~tags:(Logging.Tags.preamble "Make Replacement")
      "Attempting to create replacement for bad location: %a"
      Location.print_loc loc in
    None
  | { loc_ghost = true; _ } ->
    None
  | _ ->
    Some (mk loc p)

let location { location; _ } = location

let pp fmt { location; payload } =
  let { loc_start; loc_end; _ } = location in
  let { pos_fname; _ } = loc_start in
  Format.fprintf fmt
    "Location: %s:%i,%i(%i-%i)--%i,%i(%i-%i), Payload: %s"
    pos_fname loc_start.pos_lnum (loc_start.pos_cnum - loc_start.pos_bol)
      loc_start.pos_cnum loc_start.pos_bol
    loc_end.pos_lnum (loc_end.pos_cnum - loc_end.pos_bol)
      loc_end.pos_cnum loc_end.pos_bol
    payload

let remove_overlapping rs =
  let f r rs =
    match Set.max_elt_opt rs with
    | None ->
      Set.add r rs
    | Some r' ->
      let { loc_end = { pos_cnum = _end; _ }; _ } = location r' in
      let { loc_start = { pos_cnum = start; _}; _ } = location r in
      if start < _end then
        let () = Logging.err @@ fun _f -> _f
          ~tags:(Logging.Tags.preamble "Apply Replacement")
          "@[<v 4>Overlapping replacements:@,%a@,%a@]"
          pp r pp r' in
        rs
      else
        Set.add r rs in
  Set.fold f rs Set.empty

let apply_all rs s =
  let cur = ref { dummy_pos with pos_lnum = 1; pos_cnum = 0; pos_bol = 0; } in
  let lines = Array.of_list (String.lines s) in
  let apply r buf =
    let { location = ({ loc_start; loc_end; loc_ghost} as l); payload } = r in
    begin if loc_ghost then
      invalid_arg (Format.sprintf "Ghost location: %a" Location.print_loc l)
    else if check_loc l lines then
      let buf = lines_between_buf (!cur, loc_start) lines buf in
      Buffer.add_string buf payload ;
      cur := loc_end
    else
      Logging.err @@ fun _f -> _f
        ~tags:(Logging.Tags.preamble "Apply Replacement")
        "Replacement location not in range: %a"
        Location.print_loc l
    end ;
    buf in
  let buf =
    Set.fold apply (remove_overlapping rs) (Buffer.create (String.length s)) in
  let end_pos =
    { dummy_pos with
        pos_lnum = (Array.length lines);
        pos_cnum = String.length lines.(Array.length lines - 1);
        pos_bol = 0; } in
  let buf = lines_between_buf (!cur, end_pos) lines buf in
  if String.suffix ~suf:"\n" s then Buffer.add_char buf '\n' ;
  Buffer.contents buf

let apply r s =
  apply_all (Set.singleton r) s