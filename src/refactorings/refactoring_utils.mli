open Containers

open Compiler

open Identifier

val lookup_path :
  lookup_f:(Longident.t -> Path.t) -> ?backup_lib:string -> env:Env.t
    -> (string option * Longident.t) -> Path.t option

module InputState : sig

  module type S = sig

    val get_input : unit -> Sourcefile.t
    val set_input : Sourcefile.t -> unit

    val input_module : unit -> string
    val input_lib : unit -> string option

    val dispatch_on_input_type :
      intf_f:(Typedtree.signature -> 'a)
        -> impl_f:(Typedtree.structure -> 'a)
          -> 'a

    val find_path_to_local_binding :
      (Ident.t, 'a) Atom.t -> Identifier.Chain._t Option.t

  end

  module Make () : S

end