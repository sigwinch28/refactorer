(******************************************************************************)
(* The interface to the compiler                                              *)
(******************************************************************************)

include
  (Compiler_import
    : module type of Compiler_import [@remove_aliases]
        with module Env       := Rotor_env
         and module Ident     := Rotor_ident
         and module Location  := Rotor_location
         and module Longident := Rotor_longident
         and module Path      := Rotor_path
         and module Types     := Rotor_types
         and module Typedtree := Rotor_typedtree
         and module Typemod   := Compiler_import.Typemod
  )

module Env       = Rotor_env
module Ident     = Rotor_ident
module Location  = Rotor_location
module Longident = Rotor_longident
module Path      = Rotor_path
module Types     = Rotor_types
module Typedtree = Rotor_typedtree

module Typemod = struct
  include Compiler_import.Typemod
#if OCAML_MINOR > 8
  let type_interface f env pt = type_interface env pt
#endif
end

#if OCAML_MINOR < 8
let find_in_path f = Misc.find_in_path !Config.load_path f
let find_in_path_uncap f = Misc.find_in_path_uncap !Config.load_path f
#else
let find_in_path = Load_path.find
let find_in_path_uncap = Load_path.find_uncap
#endif

#if OCAML_MINOR < 9
let init_path ?dir () = Compmisc.init_path ?dir false
#else
let init_path = Compmisc.init_path
#endif