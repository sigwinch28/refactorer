open Compiler_import

include module type of struct include Longident end

val hd : t -> string
val tl : t -> t
val build : string list -> t
val append : t -> t -> t
