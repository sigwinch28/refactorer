open Compiler_import

include Typedtree

let env_of_exp_extra =
  function
#if OCAML_MINOR < 8
  | Texp_open (_, _, _, env) -> Some env
#endif
  | _ -> None
