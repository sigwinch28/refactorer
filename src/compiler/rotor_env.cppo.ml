open Containers

open Compiler_import
open   Types

include Env

#if OCAML_MINOR < 8
let normalize_module_path = normalize_path
#endif

let rec normalize_mty_path ?(lax=true) env path =
  let open Rotor_path in
  let path =
    match path with
    | Pident _ ->
      path
    | Pdot _ ->
      let loc = if lax then Some Location.none else None in
      dot_map ~hd:(normalize_module_path loc env) path
    | Papply(p1, p2) ->
      invalid_arg (Format.sprintf "%s.normalize_mty_env" __MODULE__) in
  try match find_modtype path env with
  | Types.{ mtd_type = Some Mty_alias _; _ } ->
    assert false
  | Types.{ mtd_type = Some Mty_ident path; _ } ->
    normalize_mty_path ~lax env path
  | _ ->
    path
  with
  | Not_found
      when
        lax ||
        (match path with
          | Path.Pident id -> not (Ident.persistent id)
          | _ -> true) ->
    path