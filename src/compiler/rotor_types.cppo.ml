open Containers

open Compiler_import

include Types

let get_module_type_path =
  function
  | Mty_ident p
#if OCAML_MINOR < 8
  | Mty_alias (_, p) ->
#else
  | Mty_alias p ->
#endif
    p
  | _ ->
    invalid_arg (Format.sprintf "%s.get_module_type_path" __MODULE__)
