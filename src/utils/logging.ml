open Containers
open Format
open Fun

open Compiler

open Logs

open Lib
open Lib.ApplyTokens

type t = Logs.src

let default_level = None

let log_file = ref None
let show_progress = ref false

let log = Src.create "refactoring.log"

include (val src_log log : LOG)

module TagDefs = struct
  let loc =
    Tag.def ~doc:"A location in a source file"
       "location" Location.print_loc
  let preamble =
    Tag.def ~doc:"A formatter used to print a preamble to the log entry"
      "preamble" (fun f fmt -> fmt f ())
end

module Tags = struct
  open TagDefs
  let mk def t = Tag.add def t Tag.empty
  let of_loc = mk loc
  let preamble s = mk preamble (fun fmt () -> Format.fprintf fmt "%s" s)
end

let reporter ?delegate () =
  let report src level ~over cont msgf =
    let cont _ = over (); cont () in
    let do_log f =
      msgf @@ fun ?header ?tags fmt ->
      let find_in_tags def =
        Option.flat_map (fun tags -> Tag.find def tags) tags in
      let loc = find_in_tags TagDefs.loc in
      let preamble = find_in_tags TagDefs.preamble in
      kfprintf cont f ("@[<v 4>[%a]%a%a%a @[<v>" ^^ fmt ^^ "@]@]@.")
        pp_level level
        (some (string |> (within |> brackets))) header
        (some (Tag.printer TagDefs.loc |> (within |> brackets))) loc
        (some (Tag.printer TagDefs.preamble |> after_char ' ')) preamble in
    if not (Src.equal src log) then
      let delegate = Option.get_or ~default:(reporter ()) delegate in
      delegate.report src level ~over cont msgf
    else if Option.is_none !log_file then
      do_log Format.err_formatter
    else
      let log_file = Option.get_exn !log_file in
      try
        IO.with_out_a log_file @@ fun log_file ->
        let f =
          make_formatter
            (fun s -> Stdlib.output log_file (Bytes.of_string s))
            (fun () -> Stdlib.flush log_file) in
        do_log f
      with Sys_error s ->
        let () =
          prerr_endline (sprintf "Error writing to log file: %s" s) in
        cont () in
  { report }

let output_progress (msgf : ('a, unit) Logs.msgf) =
  if !show_progress then
    let () =
      msgf @@ fun ?header ?tags fmt -> fprintf Format.err_formatter fmt in
    flush err_formatter ()

let progress_statement ?(log=false) msgf =
  output_progress msgf ;
  if log then info msgf

let output_progress msgf default =
  match msgf with
  | None ->
    output_progress default
  | Some msgf ->
    let () = info msgf in
    output_progress
      (fun _f -> msgf (fun ?header ?tags fmt -> _f ?header ?tags (fmt ^^ "@,")))

let minor_progress ?msgf () =
  output_progress msgf (fun _f -> _f ".")

let major_progress ?msgf () =
  output_progress msgf (fun _f -> _f " done@,")

let set_logging (lvl, f, show) =
  let () = show_progress := show in
  let () = log_file := f in
  let () = Logs.set_reporter (reporter ()) in
  let lvl =
    match lvl, f with
    | None, Some f -> Some Debug
    | _ -> lvl in
  Logs.set_level lvl

let level_opts = [
    "App",     Logs.App
  ; "Error",   Logs.Error
  ; "Warning", Logs.Warning
  ; "Info",    Logs.Info
  ; "Debug",   Logs.Debug
  ]

let cmdline_opts =
  let docs = "LOGGING OPTIONS" in
  let level =
    let doc = "The level of debug information provided." in
    let docv = "LEVEL" in
    let arg_info = Cmdliner.Arg.info ~docs ~docv ~doc ["log-level"] in
    Cmdliner.Arg.(value & opt (some (enum level_opts)) None arg_info) in
  let log_file =
    let doc = "The file in which to store logging information" in
    let docv = "FILE" in
    let arg_info = Cmdliner.Arg.info ~docs ~docv ~doc ["log-file"] in
    Cmdliner.Arg.(value & opt (some string) None arg_info) in
  let progress =
    let doc = "Show progress information." in
    Cmdliner.Arg.(value & flag & info ~docs ~doc ["show-progress"]) in
  Cmdliner.Term.(const (fun x y z -> (x, y, z)) $ level $ log_file $ progress)
