module type S = sig
  type elt
  type t
  val next_fp : t -> (elt -> t) -> t
end

module OfSet(S : Set.S)
  : S with type t = S.t
       and type elt = S.elt
= struct

  type t = S.t
  type elt = S.elt

  let next_fp start f =
    let rec iter acc xs =
      if S.is_empty xs then
        acc
      else
        let x = S.choose xs in
        let xs = S.remove x xs in
        let ys =
          if S.mem x acc then S.empty
          else f x in
        let acc = S.add x acc in
        let xs = S.union xs ys in
        iter acc xs in
    iter S.empty start

end