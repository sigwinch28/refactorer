open Containers

open Compiler

type ast =
  | Interface of Parsetree.signature option * Typedtree.signature option
  | Implementation of Parsetree.structure option * Typedtree.structure option
(** Parse trees contained in a source file. *)

type t = private {
  fileinfos   : Fileinfos.t ;
  contents    : string ;
  lines       : string Array.t ;
  ast         : ast ;
}
(** The type representing parsed source files. *)

val of_fileinfos : ?use_cache:bool -> Fileinfos.t -> t
(** Create a [Sourcefile.t] value from a [Fileinfos.t] value. *)

val get_module_deps : t -> Module.name_t list
(** Returns a list of the names of the modules upon which this source file
    depends. Throws a [Sourcefile.Invalid_argument] exception if there is no
    typed AST. *)

val process_ast :
  intf_f:((Parsetree.signature option * Typedtree.signature option) -> 'a)
    -> impl_f:((Parsetree.structure option * Typedtree.structure option) -> 'a)
      -> t -> 'a
(* [process_ast] takes as parameters continuations for processing the parsetree
   and typedtree of the input sourcefile, and executes the appropriate one. *)

val extract_src : t -> Location.t -> Location.t -> string

val diff : t -> string -> string

val initial_module_scope : t -> Modulescope.t
(* [initial_module_scope infos] returns the intial module scope corresponding
    to the file represented by [infos].
    Throws Invalid_argument if [infos] does not represent an interface file.
    Throws Failure if a typed AST cannot be extracted. *)