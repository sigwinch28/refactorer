#! /bin/bash

rlwrap ocaml \
  -init "${BASE_DIR}utils/init.ml" \
  -I "${BASE_DIR}" \
  -I "${BASE_DIR}_build/src/ast_visitors" \
  -I "${BASE_DIR}_build/src/compiler" \
  -I "${BASE_DIR}_build/src/driver" \
  -I "${BASE_DIR}_build/src/infrastructure" \
  -I "${BASE_DIR}_build/src/language" \
  -I "${BASE_DIR}_build/src/refactorings" \
  -I "${BASE_DIR}_build/src/refactorings/rename" \
  -I "${BASE_DIR}_build/src/utils" \
  -I "${BASE_DIR}_build/utils" \
  -I "${BASE_DIR}_build" \
  "$@"
