# This script takes a patch file, and checks whether the testbed compiles
# after the patch is applied. It is primarily meant to be called by other
# scripts that run batches of refactoring tests, and so assumes that certain
# environment variables are set (see comments below).

PATCH_FILE=$1

if [ -z "$PATCH_FILE" ]; then
  echo "Expecting a patch file!"
  exit 1
fi

# The script expects various environment variables to be set

if [ ! -d "$TESTBED_PATH" ]; then
  echo "Cannot find test bed!"
  exit 1
fi

if [ ! -d "$CONFIG_DIR" ]; then
  echo "Cannot find config directory!"
  exit 1
fi

if [ ! -d "$RESULTS_DIR" ]; then
  echo "Cannot find results directory!"
  exit 1
fi

# Now for the main stuff

ID=${PATCH_FILE%.patch}

TMP_DIR=`mktemp -d`
cp -R "$TESTBED_PATH" "$TMP_DIR/code"
cd "$TMP_DIR/code"
"$CONFIG_DIR/clean.sh"
patch -p 0 < "$RESULTS_DIR/$PATCH_FILE" > /dev/null 2>&1
"$CONFIG_DIR/build.sh"

if [ $? -ne 0 ]; then
  "$CONFIG_DIR/copy_build_log.sh" "$RESULTS_DIR/$ID.failed"
  echo "$ID BUILD_FAILED"
else
  # Now check the if the object code is the same
  if [ -n "$DISASSEMBLE" ]; then
    TMP1=`mktemp`
    TMP2=`mktemp`
    ALLDIFFS=""
    for OBJFILE in $OBJFILES; do
      "$DISASSEMBLE" -noloc "$TESTBED_PATH/$BUILD_SUBDIR/$OBJFILE" 2>/dev/null | grep -v "##" > "$TMP1" 2>/dev/null
      "$DISASSEMBLE" -noloc "$TMP_DIR/code/$BUILD_SUBDIR/$OBJFILE" 2>/dev/null | grep -v "##" > "$TMP2" 2>/dev/null
      DIFF=$(diff -u "$TMP1" "$TMP2" 2>&1)
      if [ $? -eq 0 ]; then
        DIFF=$(echo -e "$DIFF" | filterdiff --remove-timestamps)
      fi
      if [ -n "$DIFF" ]; then
        ALLDIFFS="$ALLDIFFS\n$OBJFILE\n$DIFF"
      fi
    done
    if [ -n "$ALLDIFFS" ]; then
      echo -e "$ALLDIFFS" > "$RESULTS_DIR/$ID.failed"
      echo "$ID OBJECT_FILES_DIFFER"
    else
      echo "$ID SUCCEEDED"
    fi
  else
    echo "$ID SUCCEEDED"
  fi
fi

rm -fR "$TMP_DIR"