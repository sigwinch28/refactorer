#use "utils/init.ml" ;;

open Frontend

let run codebase =
  codebase
    |> Codebase.iter @@ fun f
    -> let filename = f.Fileinfos.filename in
       if not (Filename.check_suffix filename !Compiler.Config.interface_suffix) then
         print_endline
           Filename.(concat (dirname filename) (Fileinfos.object_filename f))

let cmd =
  let open Cmdliner.Term in
  (preamble & (const run $ (Codebase.of_cmdline $ const !Configuration.tool_name))),
  (info "Enumerate Object Files")

let () =
  Cmdliner.Term.(exit @@ eval cmd)