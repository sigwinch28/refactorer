# This file is generated by dune, edit dune-project instead
opam-version: "2.0"
synopsis: "An automatic refactoring tool for OCaml"
description:
  "ROTOR is a tool for automatically carrying out refactoring on multi-file OCaml codebases. Currently, it supports renaming of value declarations."
maintainer: ["Reuben N. S. Rowe <reuben.rowe@cantab.net>"]
authors: [
  "Reuben N. S. Rowe <reuben.rowe@cantab.net>"
  "Hugo Férée <hugo.feree@gmail.com>"
  "Joe Harrison <joe@sigwinch.co.uk>"
  "Steven Varoumas <steven.varoumas@lip6.fr>"
]
license: "MIT"
homepage: "https://trustworthy-refactoring.gitlab.io/refactorer"
bug-reports: "https://gitlab.com/trustworthy-refactoring/refactorer/issues"
depends: [
  "cmdliner" {>= "1.0.4"}
  "cppo" {build}
  "containers" {>= "3.0"}
  "dune" {>= "1.11"}
  "ocaml" {>= "4.04.1" & < "4.10"}
  "ocamlfind" {>= "1.8.1"}
  "ocamlgraph" {>= "1.8.7"}
  "logs" {>= "0.6.2"}
  "re" {>= "1.9.0"}
  "mparser" {>= "1.2.3"}
  "visitors" {>= "20190711"}
]
build: [
  ["dune" "subst"] {pinned}
  [
    "dune"
    "build"
    "-p"
    name
    "-j"
    jobs
    "@install"
    "@runtest" {with-test}
    "@doc" {with-doc}
  ]
]
dev-repo: "git+https://github.com/trustworthy-refactoring/refactorer.git"
depexts: [
  "diffutils"
  "patchutils"
]